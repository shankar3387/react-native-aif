import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Logo from '../Logo';
import Form from '../Form';
import Wallpaper from '../Wallpaper';
import ButtonSubmit from '../ButtonSubmit';
import SignupSection from '../signup/SignupSection';

export default class LoginScreen extends Component {
    
   constructor(props){
        super(props)
        this.state = {
            oke:'kk',
            userName:'',
            password:''
        }
        this.handler = this.handler.bind(this)
   }
   handler(name) {
    this.setState(name)
  }
  render() {
    return (
      <Wallpaper>
        <Logo />
        <Form handler={this.handler} />
        <SignupSection state={this.state} />
        <ButtonSubmit  userName={this.state.userName} password={this.state.password} />
      </Wallpaper>
    );
  }
}