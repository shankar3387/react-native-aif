import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
// import Drawer from './admin/layout/sidebar/Drawer'
import LoginScreen from './commonLayout/login/LoginScreen';
import Constants from 'expo-constants';
import AdminLayout from './admin/layout/AdminLayout'
export default function App() {
  return (
<Router>
  <Scene hideNavBar key="root">
      <Scene hideNavBar key='login' component={LoginScreen} initial="true"></Scene>
      <Scene key='admin' component={AdminLayout} ></Scene>
  </Scene>
</Router>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
